echo 'Starting to clone stuffs needed to build for marble'


# Vendor
echo 'Cloning vendor tree'
rm -rf vendor/xiaomi/marble && git clone https://gitlab.com/a144643140/prebuilt_vendor_xiaomi_marble.git -b derp-14 vendor/xiaomi/marble

# Kernel
echo 'Cloning kernel tree'
git clone --depth=1 https://gitlab.com/a144643140/kernel_xiaomi_marble.git -b derp-14 kernel/xiaomi/marble

# Xiaomi
echo 'Cloning hardware xiaomi'
rm -rf hardware/xiaomi && git clone https://github.com/frozen24king/lineage_hardware_xiaomi-1.git -b lineage-20 hardware/xiaomi

echo 'delete vendorsetup.sh from device tree once this is done'
